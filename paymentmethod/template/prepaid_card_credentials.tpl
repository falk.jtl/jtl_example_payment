<fieldset>
    <input type="hidden" name="credentials[post]" value="1">
    <legend>{$pluginLocale->getTranslation('prepayed_payment_head_login')}</legend>
    <p>{$pluginLocale->getTranslation('prepayed_payment_desc_login')}</p>
    {row}
        {col cols=12 md=6}
            {formgroup label-for="prepaid_card_name"
                label={lang section="checkout" key="accountHolder"}
            }
                {input id="prepaid_card_name" type="text" name="credentials[loginName]" value=$credentials_loginName placeholder={lang section="checkout" key="accountHolder"} required=true autocomplete="username email"}
            {/formgroup}
        {/col}
        {col cols=12 md=6}
            {formgroup label-for="prepaid_card_password"
                label={lang section="global" key="password"}
            }
                {input id="prepaid_card_password" type="password" name="credentials[secret]" value=$credentials_secret placeholder={lang section="global" key="password"} required=true autocomplete="off"}
            {/formgroup}
        {/col}
    {/row}
</fieldset>
