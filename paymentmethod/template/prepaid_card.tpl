<p><strong>{$pluginLocale->getTranslation('head_info_about_your_payment')}</strong></p>
{if isset($prepaid_card_payStatus)}
<p>{$pluginLocale->getTranslation('prepayed_payment_'|cat:$prepaid_card_payStatus)}</p>
{elseif isset($additionalNeeded) && $additionalNeeded === true}
    {form id="form_payment_extra" class="evo-validate label-slide payment_extra" method="post" action=""}
        <div id="order-additional-payment" class="bottom15 form-group">
            {include file=$method->getAdditionalTemplate()}
            <input type="hidden" name="Zahlungsart" value="{$Bestellung->Zahlungsart->kZahlungsart}" />
            <input type="hidden" name="kBestellung" value="{$Bestellung->kBestellung}" />
        </div>
        <div class="text-right">
            <input type="submit" value="{lang key='payNow' section='global'}" class="submit btn btn-lg submit_once btn-primary" />
        </div>
    {/form}
{/if}
