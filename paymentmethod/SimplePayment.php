<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\paymentmethod;

use JTL\Alert\Alert;
use JTL\Backend\Notification;
use JTL\Backend\NotificationEntry;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Plugin\Data\PaymentMethod as DataPaymentMethod;
use JTL\Plugin\Helper as PluginHelper;
use JTL\Plugin\Payment\Method;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use PHPMailer\PHPMailer\Exception;
use stdClass;

/**
 * Class SimplePayment
 * @package Plugin\jtl_example_payment\paymentmethod\src
 */
class SimplePayment extends Method implements NotificationInterface
{
    protected const MAILTEMPLATE_SIMPLEPAYMENT = 'kPlugin_%d_SimplePaymentTransferData';

    /** @var int */
    protected $state = self::STATE_VALID;

    /** @var PluginInterface */
    protected $plugin;

    /** @var DataPaymentMethod */
    protected $payMethod;

    /**
     * @inheritDoc
     *
     * Wird bei jedem Instanziieren der Zahlungsmethode aufgerufen.
     * Speichert die Plugin-Instanz lokal und lädt die zugehörige Sprachdatei. Mittels Smarty->assign für pluginLocale
     * werden die Sprachvariablen des Plugins im Template zugänglich gemacht und können dort mit
     * <code>
     * $pluginLocale->getTranslation('sprachvariable')
     * </code>
     * genutzt werden.
     */
    public function init($nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);

        $pluginID        = PluginHelper::getIDByModuleID($this->moduleID);
        $this->plugin    = PluginHelper::getLoaderByPluginID($pluginID)->init($pluginID);
        $this->payMethod = $this->plugin->getPaymentMethods()->getMethodByID($this->moduleID);
        Shop::Container()->getGetText()->loadPluginLocale(
            'simple_payment',
            $this->plugin
        );
        Shop::Smarty()->assign('pluginLocale', $this->plugin->getLocalization());

        if ($this->payMethod !== null) {
            $this->kZahlungsart = $this->payMethod->getMethodID();
        }

        return $this;
    }

    /**
     * Wird vom Bootstraper aufgerufen, um evtl. Notifications im Backend anzuzeigen
     *
     * @return void
     */
    public function initBackendNotification(): void
    {
        if (Shop::isFrontend() || $this->isValidIntern()) {
            return;
        }

        $stateMsg = $this->getStateMessage($this->state);
        if (!empty($stateMsg)) {
            $note = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                __('JTL Example Payment'),
                \sprintf($stateMsg, __($this->caption)),
                Shop::getAdminURL() . '/zahlungsarten.php?kZahlungsart=' . $this->kZahlungsart
                    . '&token=' . $_SESSION['jtl_token']
            );
            $note->setPluginId($this->plugin->getID());
            Notification::getInstance()->addNotify($note);
        }
    }

    /**
     * @inheritDoc
     */
    public function getStateMessage(int $state): string
    {
        if ($state === self::STATE_DURING_CHECKOUT) {
            return __('The payment method is not usable before order completion');
        }

        return '';
    }

    /**
     * @inheritDoc
     *
     * Interne Validierung der Zahlungsmethode. Dies wird im Frontend beim Prüfen der gewählten Zahlungsart
     * und durch die init-Methode im Backend aufgerufen.
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if ($this->duringCheckout) {
            $this->state = self::STATE_DURING_CHECKOUT;

            return false;
        }

        return parent::isValidIntern($args_arr);
    }

    /**
     * @inheritDoc
     *
     * Diese Methode wird durch den Bestellabschluss nach Finalisierung der Bestellung aufgerufen und startet den
     * Bezahlvorgang der Zahlungsmethode.
     * Lädt das über die info.xml definierte E-Mail-Template für die Zahlungsmethode und versendet dieses über den
     * Mailer-Service des Shops.
     */
    public function preparePaymentProcess($order): void
    {
        parent::preparePaymentProcess($order);

        $obj              = new stdClass();
        $obj->tkunde      = Frontend::getCustomer();
        $obj->tbestellung = $order;
        $tplKey           = \sprintf(self::MAILTEMPLATE_SIMPLEPAYMENT, $this->plugin->getID());

        /** @var Mailer $mailer */
        $mailer = Shop::Container()->get(Mailer::class);
        $mailer->getHydrator()->add('Bestellung', $order);

        $mail = new Mail();
        try {
            $mailer->send($mail->createFromTemplateID($tplKey, $obj));
        } catch (Exception $e) {
        } catch (\SmartyException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                __('Payment mail for Simple payment cant be send'),
                'simplePaymentCantSendMail'
            );
        }
    }
}
