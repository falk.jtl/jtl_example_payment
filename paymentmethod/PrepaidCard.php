<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\paymentmethod;

use Exception;
use JTL\Alert\Alert;
use JTL\Backend\Notification;
use JTL\Backend\NotificationEntry;
use JTL\DB\ReturnType;
use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Plugin\Data\PaymentMethod as DataPaymentMethod;
use JTL\Plugin\Helper as PluginHelper;
use JTL\Plugin\Payment\Method;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_example_payment\src\ServiceProvider;
use Plugin\jtl_example_payment\src\ServiceProviderException;
use StringHandler;

/**
 * Class PrepaidCard
 * @package Plugin\jtl_example_payment\paymentmethod
 */
class PrepaidCard extends Method implements NotificationInterface
{
    protected const USERCREDENTIALS = 'jtl_example_payment.user.credentials.prepaid';

    /** @var PluginInterface */
    protected $plugin;

    /** @var DataPaymentMethod */
    protected $payMethod;

    /** @var int */
    protected $state = self::STATE_VALID;

    public const PAYSTATUS_SUCCESS = 'done';
    public const PAYSTATUS_PARTIAL = 'partial';
    public const PAYSTATUS_FAILED  = 'failed';

    /**
     * @inheritDoc
     *
     * Wird bei jedem Instanziieren der Zahlungsmethode aufgerufen.
     * Speichert die Plugin-Instanz lokal und lädt die zugehörige Sprachdatei. Mittels Smarty->assign für pluginLocale
     * werden die Sprachvariablen des Plugins im Template zugänglich gemacht und können dort mit
     * <code>
     * $pluginLocale->getTranslation('sprachvariable')
     * </code>
     * genutzt werden.
     */
    public function init($nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);

        $pluginID        = PluginHelper::getIDByModuleID($this->moduleID);
        $this->plugin    = PluginHelper::getLoaderByPluginID($pluginID)->init($pluginID);
        $this->payMethod = $this->plugin->getPaymentMethods()->getMethodByID($this->moduleID);
        Shop::Container()->getGetText()->loadPluginLocale(
            'prepaid_card',
            $this->plugin
        );
        Shop::Smarty()->assign('pluginLocale', $this->plugin->getLocalization());

        if ($this->payMethod !== null) {
            $this->kZahlungsart = $this->payMethod->getMethodID();
        }

        return $this;
    }

    /**
     * Wird vom Bootstraper aufgerufen, um evtl. Notifications im Backend anzuzeigen
     *
     * @return void
     */
    public function initBackendNotification(): void
    {
        if (Shop::isFrontend() || $this->isValidIntern()) {
            return;
        }

        $stateMsg = $this->getStateMessage($this->state);
        if (!empty($stateMsg)) {
            $note = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                __('JTL Example Payment'),
                \sprintf($stateMsg, __($this->caption)),
                'zahlungsarten.php?kZahlungsart=' . $this->kZahlungsart . '&token=' . $_SESSION['jtl_token']
            );
            $note->setPluginId($this->plugin->getID());
            Notification::getInstance()->addNotify($note);
        }
    }

    /**
     * @inheritDoc
     */
    public function getStateMessage(int $state): string
    {
        switch ($state) {
            case self::STATE_DURING_CHECKOUT:
                return __('The payment method is not usable before order completion');
            case self::STATE_NOT_CONFIGURED:
            default:
                return __('The payment method is not configured');
        }
    }

    /**
     * @inheritDoc
     *
     * Interne Validierung der Zahlungsmethode. Dies wird im Frontend beim Prüfen der gewählten Zahlungsart
     * und durch die init-Methode im Backend aufgerufen.
     */
    public function isValidIntern($args_arr = []): bool
    {
        if (empty($this->getSetting('prepaid_card_provider_url'))
            || empty($this->getSetting('prepaid_card_merchant_login'))
            || empty($this->getSetting('prepaid_card_merchant_secret'))
        ) {
            $this->state = self::STATE_NOT_CONFIGURED;

            return false;
        }

        if ($this->duringCheckout) {
            $this->state = self::STATE_DURING_CHECKOUT;

            return false;
        }

        return parent::isValidIntern($args_arr);
    }

    /**
     * Setzt das Verifizierungstoken zurück.
     *
     * @return void
     */
    protected function resetToken(): void
    {
        $credentials = Frontend::get(self::USERCREDENTIALS, []);
        unset($credentials['token']);
        Frontend::set(self::USERCREDENTIALS, $credentials);
    }

    /**
     * @inheritDoc
     *
     * Durch das Überschreiben der Standardmethode für das Abfrufen der eigenen Settings wird der Zugriff
     * vereinfacht, da automatisch die Module-ID vorangestellt wird.
     */
    public function getSetting($key)
    {
        $key = $this->moduleID . '_' . $key;

        return $this->plugin->getConfig()->getValue($key);
    }

    /**
     * Diese Methode prüft beim Service-Provider die Gültigkeit der Anmeldedaten.
     *
     * @param string $customerName
     * @param string $secret
     * @return string
     */
    public function validateCredentials(string $customerName, string $secret): string
    {
        try {
            $serviceProvider = new ServiceProvider((string)$this->getSetting('prepaid_card_provider_url'));

            return $serviceProvider->checkCustomer($customerName, $secret);
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'validateCredentials'
            );

            return '';
        }
    }

    /**
     * Diese Methode prüft beim Service-Provider ob genügend Guthaben vorhanden ist.
     *
     * @param float $creditSum
     * @return bool
     */
    public function validateCredit(float $creditSum): bool
    {
        $credentials = Frontend::get(self::USERCREDENTIALS, []);

        if (empty($credentials['name']) || empty($credentials['token'])) {
            return false;
        }

        try {
            $serviceProvider = new ServiceProvider((string)$this->getSetting('prepaid_card_provider_url'));

            return $serviceProvider->checkCustomerCredit($credentials['token'], $creditSum);
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'validateCredit'
            );
        }

        return false;
    }

    /**
     * @inheritDoc
     *
     * Wird im Bestellprozess aufgerufen und entscheidet im Zusammenspiel mit handleAdditional ob das
     * Zusatzschritt-Template nach der Auswahl der Zahlungsart angezeigt werden muss. Diese Methode prüft anhand
     * von Post- und Session-Daten, ob gültige Anmeldeinformationen vorliegen und speichert diese ggfs. in der
     * Session für die weitere Verarbeitung.
     * Konnten die Daten nicht valiidert werden wird FALSE zurückgegeben, ansonsten TRUE.
     */
    public function validateAdditional(): bool
    {
        $credentials     = Frontend::get(self::USERCREDENTIALS, []);
        $postCredentials = Request::postVar('credentials', []);

        if (Request::getInt('editZahlungsart') > 0 || Request::getInt('editVersandart') > 0) {
            $this->resetToken();

            return false;
        }

        if (isset($postCredentials['post'])) {
            if (!Form::validateToken()) {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    Shop::Lang()->get('invalidToken'),
                    'invalidToken'
                );

                return false;
            }

            $secret               = StringHandler::filterXSS($postCredentials['secret']);
            $credentials['name']  = StringHandler::filterXSS($postCredentials['loginName']);
            $credentials['token'] = $this->validateCredentials($credentials['name'], $secret);

            Frontend::set(self::USERCREDENTIALS, $credentials);

            return !empty($credentials['token']);
        }

        if (!empty($credentials['token'])) {
            return parent::validateAdditional();
        }

        return false;
    }

    /**
     * @inheritDoc
     *
     * Wird im Bestellprozess aufgerufen, um zu prüfen ob der Zusatzschritt im Checkout angezeigt werden soll.
     * Ist der Zwischenschritt aus Plugin-Sicht notwendig, muss FALSE zurückgegeben werden.
     * Der Zwischenschritt wird hier angezeigt, wenn die Zahlungsart (neu) gewählt wurde oder noch keine
     * Nutzer-Credentials vorliegen.
     */
    public function handleAdditional($post): bool
    {
        $credentials = Frontend::get(self::USERCREDENTIALS, []);

        if (empty($credentials['name']) || empty($credentials['token'])) {
            Shop::Smarty()
                ->assign('credentials_loginName', empty($credentials['name'])
                    ? Frontend::getCustomer()->cMail
                    : $credentials['name'])
                ->assign('credentials_secret', '')
                ->assign('additionalNeeded', true);

            return false;
        }

        return parent::handleAdditional($post);
    }

    /**
     * @inheritDoc
     *
     * Hier wird festgelegt, ob die Bezahlung über das Plugin erneut gestartet werden kann. Gibt diese Methode TRUE
     * zurück, dann wird bei einer unbezahlten Rechnung im Kundenaccount ein "Jetzt bezahlen"-Link angezeigt.
     */
    public function canPayAgain(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     *
     * Diese Methode wird durch den Bestellabschluss nach Finalisierung der Bestellung oder einem erneuten starten
     * des Bezahlvorganges @see canPayAgain aufgerufen und startet den Bezahlvorgang der Zahlungsmethode.
     * Über den Service-Provider wird hier eine Zahlung veranlasst und im Erfolgsfall ein Zahlungseingang persistiert.
     * Konnte die Bestellung vollständig bezahlt werden, wird die Bestellung auf "bezahlt" gesetzt.
     */
    public function preparePaymentProcess($order): void
    {
        parent::preparePaymentProcess($order);

        $credentials     = Frontend::get(self::USERCREDENTIALS, []);
        $serviceProvider = new ServiceProvider((string)$this->getSetting('prepaid_card_provider_url'));
        try {
            $forcePay  = (int)$this->getSetting('prepaid_card_partial_payment') > 0;
            $payStatus = self::PAYSTATUS_FAILED;
            $payValue  = round((float)Shop::Container()->getDB()->queryPrepared(
                'SELECT :total - COALESCE(SUM(fBetrag), 0) amount
                    FROM tzahlungseingang
                    WHERE kBestellung = :orderID',
                [
                    'total'   => (float)$order->fGesamtsumme,
                    'orderID' => (int)$order->kBestellung
                ],
                ReturnType::SINGLE_OBJECT
            )->amount, 4);

            if ($payValue <= 0) {
                $this->setOrderStatusToPaid($order);

                return;
            }

            if (!$forcePay && !$serviceProvider->checkCustomerCredit(
                $credentials['token'],
                $payValue
            )) {
                $this->resetToken();
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    $this->plugin->getLocalization()->getTranslation('prepayed_payment_not_enough_credit'),
                    'paymentFailed'
                );
            } else {
                $hash    = $this->generateHash($order);
                $payment = $serviceProvider->payPrepaidTransaction(
                    'PrepaidPayment: ' . $hash,
                    (string)$this->getSetting('prepaid_card_merchant_login'),
                    (string)$this->getSetting('prepaid_card_merchant_secret'),
                    $credentials['token'],
                    '',
                    $payValue,
                    $forcePay
                );

                if ($payment->payment_value >= $payValue) {
                    $payStatus = self::PAYSTATUS_SUCCESS;
                } else {
                    $payStatus = self::PAYSTATUS_PARTIAL;
                }

                if ($payment->payment_value > 0) {
                    $this->deletePaymentHash($hash);
                    $this->addIncomingPayment($order, (object)[
                        'fBetrag'  => $payment->payment_value,
                        'cZahler'  => $credentials['name'],
                        'cHinweis' => $payment->payment_key,
                    ]);
                }

                if ($payStatus === self::PAYSTATUS_SUCCESS) {
                    $this->setOrderStatusToPaid($order);
                }
            }

            Shop::Smarty()->assign('prepaid_card_payStatus', $payStatus);
        } catch (ServiceProviderException $e) {
            $this->resetToken();
            Shop::Smarty()->assign('prepaid_card_payStatus', self::PAYSTATUS_FAILED);
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'paymentFailed'
            );
        }
    }
}
