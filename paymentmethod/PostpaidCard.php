<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\paymentmethod;

use JTL\Alert\Alert;
use JTL\Backend\Notification;
use JTL\Backend\NotificationEntry;
use JTL\Checkout\Bestellung;
use JTL\Customer\Customer;
use JTL\DB\ReturnType;
use JTL\Helpers\Request;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Plugin\Data\PaymentMethod as DataPaymentMethod;
use JTL\Plugin\Helper as PluginHelper;
use JTL\Plugin\Payment\Method;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_example_payment\src\ServiceProvider;
use StringHandler;

/**
 * Class PostpaidCard
 * @package Plugin\jtl_example_payment\paymentmethod
 */
class PostpaidCard extends Method implements NotificationInterface
{
    protected const USERCREDENTIALS = 'jtl_example_payment.user.credentials.postpaid';
    protected const DATACONTEXT     = 'jtl_example_payment';

    protected const MAILTEMPLATE_PAYMENTCANCEL = 'kPlugin_%d_PostPaidPaymentCancel';

    /** @var PluginInterface */
    protected $plugin;

    /** @var DataPaymentMethod */
    protected $payMethod;

    /** @var int */
    protected $state = self::STATE_VALID;

    /** @var bool */
    protected $payAgain;

    /** @var string */
    protected $payStatus = '';

    public const PAYSTATUS_SUCCESS = 'done';
    public const PAYSTATUS_FAILED  = 'failed';
    public const PAYSTATUS_PAID    = 'paid';
    public const PAYSTATUS_CANCEL  = 'cancel';

    /**
     * @inheritDoc
     *
     * Wird bei jedem Instanziieren der Zahlungsmethode aufgerufen.
     * Speichert die Plugin-Instanz lokal und lädt die zugehörige Sprachdatei. Mittels Smarty->assign für pluginLocale
     * werden die Sprachvariablen des Plugins im Template zugänglich gemacht und können dort mit
     * <code>
     * $pluginLocale->getTranslation('sprachvariable')
     * </code>
     * genutzt werden.
     */
    public function init(int $nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);

        $pluginID        = PluginHelper::getIDByModuleID($this->moduleID);
        $this->plugin    = PluginHelper::getLoaderByPluginID($pluginID)->init($pluginID);
        $this->payMethod = $this->plugin->getPaymentMethods()->getMethodByID($this->moduleID);
        $this->payAgain  = $nAgainCheckout > 0;
        Shop::Container()->getGetText()->loadPluginLocale(
            'postpaid_card',
            $this->plugin
        );
        Shop::Smarty()->assign('pluginLocale', $this->plugin->getLocalization());

        if ($this->payMethod !== null) {
            $this->kZahlungsart = $this->payMethod->getMethodID();
        }

        return $this;
    }

    /**
     * Wird vom Bootstraper aufgerufen, um evtl. Notifications im Backend anzuzeigen
     *
     * @return void
     */
    public function initBackendNotification(): void
    {
        if (Shop::isFrontend() || $this->isValidIntern()) {
            return;
        }

        $stateMsg = $this->getStateMessage($this->state);
        if (!empty($stateMsg)) {
            $note = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                __('JTL Example Payment'),
                \sprintf($stateMsg, __($this->caption)),
                'zahlungsarten.php?kZahlungsart=' . $this->kZahlungsart . '&token=' . $_SESSION['jtl_token']
            );
            $note->setPluginId($this->plugin->getID());
            Notification::getInstance()->addNotify($note);
        }
    }

    /**
     * @inheritDoc
     */
    public function getStateMessage(int $state): string
    {
        switch ($state) {
            case self::STATE_DURING_CHECKOUT:
                return __('The payment method is not usable before order completion');
            case self::STATE_NOT_CONFIGURED:
            default:
                return __('The payment method is not configured');
        }
    }

    /**
     * Setzt das Verifizierungstoken zurück.
     *
     * @return void
     */
    protected function resetToken(): void
    {
        $credentials = Frontend::get(self::USERCREDENTIALS, []);
        unset($credentials['token']);
        Frontend::set(self::USERCREDENTIALS, $credentials);
    }

    /**
     * Diese Methode ermittelt die gespeicherten Hash-Werte der Zahlungsmethode
     * cId:     Zahlungs-ID, die auch in tbestellid als Zuordnung zur Bestellung gespeichert wird
     * txn_id:  Payment-ID des Zahlungsanbieter - wenn diese vorliegt,
     *          wurde die Zahlung erfolgreich beim Anbieter registriert
     *
     * @param int $orderID
     * @return object|null
     */
    protected function getPaymentHashes(int $orderID): ?object
    {
        if ($orderID > 0) {
            $hashes = Shop::Container()->getDB()->queryPrepared(
                'SELECT cId, txn_id
                    FROM tzahlungsid
                    WHERE kBestellung = :orderID',
                [
                    'orderID' => $orderID
                ],
                ReturnType::SINGLE_OBJECT
            );

            return $hashes === false ? null : $hashes;
        }

        return null;
    }

    /**
     * Prüft und ermittelt die Daten, die in den POST-Daten im eigenen Context übermittelt wurden.
     * Durch den Context soll sichergestellt werden, dass eigene POST-Variablen sauber von anderen Werten
     * getrennt werden.
     *
     * @param string $task
     * @return object|null
     */
    protected function getPostContext(string $task): ?object
    {
        $result = Request::postVar(self::DATACONTEXT, '');

        if (empty($result['response'])) {
            return null;
        }

        $result = json_decode($result['response'], false);

        return $result->task === $task ? $result : null;
    }

    /**
     * @inheritDoc
     *
     * Durch das Überschreiben der Standardmethode für das Abfrufen der eigenen Settings wird der Zugriff
     * vereinfacht, da automatisch die Module-ID vorangestellt wird.
     */
    public function getSetting($key)
    {
        $key = $this->moduleID . '_' . $key;

        return $this->plugin->getConfig()->getValue($key);
    }

    /**
     * @inheritDoc
     *
     * Interne Validierung der Zahlungsmethode. Dies wird im Frontend beim Prüfen der gewählten Zahlungsart
     * und durch die init-Methode im Backend aufgerufen.
     */
    public function isValidIntern($args_arr = []): bool
    {
        if (empty($this->getSetting('postpaid_card_provider_url'))
            || empty($this->getSetting('postpaid_card_login_url'))
            || empty($this->getSetting('postpaid_card_merchant_login'))
            || empty($this->getSetting('postpaid_card_merchant_secret'))
        ) {
            $this->state = self::STATE_NOT_CONFIGURED;

            return false;
        }

        if ($this->duringCheckout) {
            $this->state = self::STATE_DURING_CHECKOUT;

            return false;
        }

        return parent::isValidIntern($args_arr);
    }

    /**
     * @inheritDoc
     *
     * Ermittelt - je nach aktuellem Status - die Return-URL, die an den Zahlungsanbieter als Rücksprungadresse
     * übergeben wird.
     */
    public function getReturnURL(?Bestellung $order): string
    {
        if ($this->payAgain) {
            return Shop::Container()->getLinkService()->getStaticRoute('bestellab_again.php')
                . '?kBestellung=' . Request::getInt('kBestellung');
        }
        if ($order === null) {
            return Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php');
        }

        $hashes = $this->getPaymentHashes($order->kBestellung);
        if (isset($hashes) && !empty($hashes->cId) && empty($hashes->txn_id)) {
            return Shop::getURL() . '/includes/modules/notify.php?ph=' . $hashes->cId;
        }

        return parent::getReturnURL($order);
    }

    /**
     * @inheritDoc
     *
     * Ermittelt - je nach aktuellem Status - die URL, die vom Zahlungsanbieter verwendet werden soll, um eine
     * zeitversetzte Zahlungsbestätigung zu signalisieren. (Webhook)
     */
    public function getNotificationURL(string $hash): string
    {
        $url = parent::getNotificationURL($hash);

        return $url . (strpos($url, '?') === false ? '?' : '&') . 'payState=1';
    }

    /**
     * @inheritDoc
     *
     * Wird im Bestellprozess aufgerufen, um zu prüfen ob der Zusatzschritt im Checkout angezeigt werden soll.
     * Ist der Zwischenschritt aus Plugin-Sicht notwendig, muss FALSE zurückgegeben werden.
     * Als Zwischenschritt findet hier eine Weiterleitung zum Service-Provider statt.
     */
    public function handleAdditional($post): bool
    {
        if ($this->payAgain && Request::getVar(self::DATACONTEXT, '') === 'cancel') {
            header(
                'Location: ' . Shop::Container()->getLinkService()->getStaticRoute('jtl.php')
                . '?bestellung=' . Request::getInt('kBestellung'),
                true,
                302
            );

            exit;
        }

        $credentials = Frontend::get(self::USERCREDENTIALS, []);

        if (empty($credentials['token'])) {
            $params = (object)['data' => [
                'customer'  => empty($credentials['name']) ? Frontend::getCustomer()->cMail : $credentials['name'],
                'returnURL' => $this->getReturnURL(null),
                'context'   => self::DATACONTEXT,
            ]];
            header(
                'Location: ' . $this->getSetting('postpaid_card_login_url') . '?params=' . json_encode($params),
                true,
                302
            );

            exit;
        }

        return parent::handleAdditional($post);
    }

    /**
     * @inheritDoc
     *
     * Wird im Bestellprozess aufgerufen und entscheidet im Zusammenspiel mit handleAdditional ob das
     * Zusatzschritt-Template nach der Auswahl der Zahlungsart angezeigt werden muss. Diese Methode prüft anhand
     * von Post- und Session-Daten, ob gültige Anmeldeinformationen vorliegen und speichert diese ggfs. in der
     * Session für die weitere Verarbeitung.
     * Konnten die Daten nicht valiidert werden wird FALSE zurückgegeben, ansonsten TRUE.
     */
    public function validateAdditional(): bool
    {
        $credentials = Frontend::get(self::USERCREDENTIALS, []);

        if (!empty($credentials['token'])) {
            return true;
        }

        $providerData = $this->getPostContext('login');

        if (!isset($providerData) || $providerData->result !== 'ok') {
            return false;
        }

        Frontend::set(self::USERCREDENTIALS, ['token' => $providerData->token]);

        return true;
    }

    /**
     * @inheritDoc
     *
     * Hier wird festgelegt, ob die Bezahlung über das Plugin erneut gestartet werden kann. Gibt diese Methode TRUE
     * zurück, dann wird bei einer unbezahlten Rechnung im Kundenaccount ein "Jetzt bezahlen"-Link angezeigt.
     */
    public function canPayAgain(): bool
    {
        $orderID = Request::getInt('bestellung');
        if ($orderID > 0) {
            $hashes = $this->getPaymentHashes($orderID);

            return !isset($hashes) || empty($hashes->txn_id);
        }

        return parent::canPayAgain();
    }

    /**
     * Diese Methode wird als Callback nach der Einlieferung beim Zahlungsanbieter aufgerufen. Je nach Status wird:
     * - im Erfolgsfall die Transaktions-ID vom Zahlungsanbieter als txn_id gespeichert
     * - im Fehlerfall das Login-Token zurückgesetzt und die Zahlungs-ID gelöscht, damit der Zahlungsvorgang
     *   erneut gestartet werden kann
     *
     * @param string $paymentHash
     * @return void
     */
    protected function finishPayment(string $paymentHash): void
    {
        $paymentData = $this->getPostContext('payTransaction');

        if (isset($paymentData) && $paymentData->result === 'ok') {
            $this->payStatus = self::PAYSTATUS_SUCCESS;
            Shop::Container()->getDB()->update('tzahlungsid', 'cId', $paymentHash, (object)[
                'txn_id' => $paymentData->token,
            ]);
            Shop::Smarty()->assign('postpaid_card_payStatus', self::PAYSTATUS_SUCCESS);
        } else {
            $this->payStatus = self::PAYSTATUS_FAILED;
            $this->resetToken();
            $this->deletePaymentHash($paymentHash);
            $errMsg = __('Call to remote payment provider failed') . ' (' . __($paymentData->msg ?? 'no message') . ')';
            Shop::Smarty()->assign('postpaid_card_payStatus', self::PAYSTATUS_FAILED);
            Shop::Container()->getAlertService()->addAlert(Alert::TYPE_ERROR, $errMsg, 'paymentFailed');
            $this->doLog($errMsg, LOGLEVEL_ERROR);
        }
    }

    /**
     * Diese Methode wird als Callback nach der Zahlungsbestätigung durch den Zahlungsanbieter aufgerufen.
     * - die Zahlungs-ID wird gelöscht
     * - ein Zahlungseingang wird angelegt
     * - die Bestellung wird auf "Bezahlt" gesetzt
     * - die Zahlungsbestätigungsmail wird verschickt
     *
     * @param Bestellung $order
     * @param string     $paymentHash
     * @param array      $payState
     * @return void
     */
    protected function acceptPayment(Bestellung $order, string $paymentHash, array $payState): void
    {
        $this->payStatus = self::PAYSTATUS_PAID;
        $this->deletePaymentHash($paymentHash);
        $this->addIncomingPayment($order, (object)[
            'fBetrag'  => (float)($payState['payment_value'] ?? $order->fGesamtsumme),
            'cZahler'  => $payState['payer_name'] ?? __('unknown'),
            'cHinweis' => $payState['payment_key'] ?? '',
        ]);
        $this->setOrderStatusToPaid($order);
        $this->sendConfirmationMail($order);
    }

    /**
     * Diese Methode wird als Callback nach dem Ablehnen der Zahlung durch den Zahlungsanbieter aufgerufen.
     * - die Zahlungs-ID wird gelöscht und neu erstellt
     * - eine E-Mail an den Kunden wird versendet
     *
     * @param Bestellung $order
     * @param string     $paymentHash
     * @param array      $payState
     * @return void
     */
    protected function revokePayment(Bestellung $order, string $paymentHash, array $payState): void
    {
        $this->payStatus = self::PAYSTATUS_CANCEL;
        $this->deletePaymentHash($paymentHash);

        $orderHash = (object)[
            'kBestellung' => $order->kBestellung,
            'dDatum'      => $order->dErstellt,
            'cId'         => uniqid('', true),
        ];
        Shop::Container()->getDB()->upsert('tbestellid', $orderHash);
        $this->doLog(sprintf(
            __('payment cancelled by provider'),
            $order->kBestellung,
            (float)($payState['payment_value'] ?? $order->fGesamtsumme),
            $payState['payment_key'] ?? ''
        ));
        $this->sendMail($order->kBestellung, self::MAILTEMPLATE_PAYMENTCANCEL, $payState);
    }

    /**
     * @inheritDoc
     *
     * Diese Methode wird als Callback (über notify.php - @see self::getNotificationURL) vom Zahlungsanbieter aufgerufen
     */
    public function handleNotification(Bestellung $order, string $hash, array $args): void
    {
        parent::handleNotification($order, $hash, $args);

        if (!\in_array($_SERVER['REQUEST_METHOD'], ['GET', 'POST'])) {
            return;
        }

        if (isset($args[self::DATACONTEXT])) {
            $this->finishPayment($hash);
        }

        if (isset($args['payState']) && $args['payState']) {
            if (isset($args['acceptPayment'])) {
                $this->acceptPayment($order, $hash, StringHandler::filterXSS($args['acceptPayment'] ?? []));
            } elseif (isset($args['deletePayment'])) {
                $this->revokePayment($order, $hash, StringHandler::filterXSS($args['deletePayment'] ?? []));
            }
        }
    }

    /**
     * @inheritDoc
     *
     * Wenn diese Methode true liefert, dann wird von notify.php auf die von @see self::getReturnURL gelieferte URL
     * weitergeleitet.
     * Hier ist das nur der Fall, wenn der aktuelle Status ein Zahlungsstatus ist, egal ob positiv oder negativ.
     */
    public function redirectOnPaymentSuccess(): bool
    {
        return \in_array($this->payStatus, [self::PAYSTATUS_FAILED, self::PAYSTATUS_SUCCESS], true);
    }

    /**
     * @inheritDoc
     *
     * Diese Methode versendet vordefinierte E-Mails. Der $type ist dabei der Name eines E-Mail-Templates aus dem
     * Shop-Standard oder ein vom Plugin angelegtes.
     * Hier wird zusätzlich das Plugin-E-Mail-Template für die Ablehnung der Zahlung behandelt.
     */
    public function sendMail(int $orderID, string $type, $additional = null)
    {
        $order = new Bestellung($orderID);
        $order->fuelleBestellung(false);
        $mailer = Shop::Container()->get(Mailer::class);

        switch ($type) {
            case self::MAILTEMPLATE_PAYMENTCANCEL:
                $data = (object)[
                    'tkunde'      => new Customer($order->kKunde),
                    'tbestellung' => $order,
                ];
                if ($data->tkunde->cMail !== '') {
                    $mailer->getHydrator()->add('Bestellung', $order);
                    $mailer->send((new Mail())->createFromTemplateID(\sprintf($type, $this->plugin->getID()), $data));
                }
                break;
            default:
                return parent::sendMail($orderID, $type, $additional);
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * Diese Methode wird durch den Bestellabschluss nach Finalisierung der Bestellung oder einem erneuten starten
     * des Bezahlvorganges @see self::canPayAgain aufgerufen und startet den Bezahlvorgang der Zahlungsmethode.
     * Per http-Weiterleitung wird hier die Seite des Zahlungs-Anbieters aufgerufen.
     */
    public function preparePaymentProcess($order): void
    {
        parent::preparePaymentProcess($order);

        $credentials     = Frontend::get(self::USERCREDENTIALS, []);
        $serviceProvider = new ServiceProvider((string)$this->getSetting('postpaid_card_provider_url'));
        $hashes          = $this->getPaymentHashes($order->kBestellung);
        $payValue        = (float)$order->fGesamtsumme;

        if (isset($hashes) && !empty($hashes->cId)) {
            $this->finishPayment($hashes->cId);

            return;
        }
        if ($payValue <= 0) {
            $this->setOrderStatusToPaid($order);

            return;
        }

        $hash   = $this->generateHash($order);
        $params = (object)['data' => [
            'transactionName' => 'PostpaidPayment: ' . $hash,
            'merchantID'      => $this->getSetting('postpaid_card_merchant_login'),
            'customerHash'    => $credentials['token'],
            'notification'    => urlencode($this->getNotificationURL($hash)),
            'value'           => $payValue,
            'returnURL'       => $this->getReturnURL($order),
            'context'         => self::DATACONTEXT,
            'task'            => 'payTransaction',
            'token'           => $serviceProvider->getTransaktionToken(
                'PostpaidPayment: ' . $hash,
                (string)$this->getSetting('postpaid_card_merchant_login'),
                (string)$this->getSetting('postpaid_card_merchant_secret'),
                $credentials['token'],
                $payValue
            ),
        ]];
        header(
            'Location: ' . $this->getSetting('postpaid_card_login_url') . '?params=' . json_encode($params),
            true,
            302
        );
        exit;
    }
}
