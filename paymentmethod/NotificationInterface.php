<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\paymentmethod;

/**
 * Interface NotificationInterface
 * @package Plugin\jtl_example_payment\paymentmethod
 */
interface NotificationInterface
{
    public const STATE_VALID           = 0;
    public const STATE_NOT_CONFIGURED  = 0x01;
    public const STATE_DURING_CHECKOUT = 0x02;
    public const STATE_INVALID         = 0xFF;

    /**
     * Wird vom Bootstraper aufgerufen, um evtl. Notifications im Backend anzuzeigen
     *
     * @return void
     */
    public function initBackendNotification(): void;

    /**
     * Liefert die Meldung zu einem speziellen Status
     *
     * @param int $state
     * @return string
     */
    public function getStateMessage(int $state): string;
}
