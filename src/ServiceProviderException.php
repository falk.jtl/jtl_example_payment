<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\src;

/**
 * Class ServiceProviderException
 * @package Plugin\jtl_example_payment\src
 */
class ServiceProviderException extends \Exception
{

}
