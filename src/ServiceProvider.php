<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\src;

use JTL\Helpers\Request;
use JTL\Session\Frontend;
use JTL\Shop;

/**
 * Class ServiceProvider
 * @package Plugin\jtl_example_payment\src
 *
 * Hilfsklasse, die über HTTP-Request mit dem Service-Provider (in dem Fall das Plugin "JTL Example Payment (Server)"
 * kommuniziert und für die Zahlunsmethoden entsprechende Wrapper-Funktionen bereitstellt.
 */
class ServiceProvider
{
    protected const REQUESTCOOKIE = 'jtl_example_payment.request.cookie';

    /** @var string */
    protected $paymentProviderURL;

    /**
     * ServiceProvider constructor.
     * @param string $paymentProviderURL
     */
    public function __construct(string $paymentProviderURL)
    {
        $this->paymentProviderURL = $paymentProviderURL;
    }

    /**
     * @param string $method
     * @param array  $data
     * @param int    $timeout
     * @return mixed
     * @throws ServiceProviderException
     */
    protected function httpRequest(string $method, array $data, int $timeout = 5)
    {
        $url = $this->paymentProviderURL;

        if (empty($url)) {
            throw new ServiceProviderException(__('Provider URL is not configured'));
        }

        $url    .= '?method=' . $method;
        $post    = 'params=' . \json_encode($data);
        $cookie  = Frontend::get(self::REQUESTCOOKIE, '');
        $headers = [];

        if (\function_exists('curl_init')) {
            $curl = \curl_init();

            \curl_setopt($curl, \CURLOPT_URL, $url);
            \curl_setopt($curl, \CURLOPT_CONNECTTIMEOUT, $timeout);
            \curl_setopt($curl, \CURLOPT_TIMEOUT, $timeout);
            \curl_setopt($curl, \CURLOPT_RETURNTRANSFER, true);
            \curl_setopt($curl, \CURLOPT_SSL_VERIFYPEER, \DEFAULT_CURL_OPT_VERIFYPEER);
            \curl_setopt($curl, \CURLOPT_SSL_VERIFYHOST, \DEFAULT_CURL_OPT_VERIFYHOST);
            \curl_setopt($curl, \CURLOPT_REFERER, Shop::getURL());
            \curl_setopt($curl, \CURLOPT_POST, true);
            \curl_setopt($curl, \CURLOPT_POSTFIELDS, $post);
            if (!empty($cookie)) {
                \curl_setopt($curl, \CURLOPT_COOKIE, $cookie);
            }
            \curl_setopt($curl, \CURLOPT_HEADERFUNCTION, static function ($curl, $header) use (&$headers) {
                $len    = \strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) {
                    return $len;
                }

                $headers[strtolower(trim($header[0]))] = trim($header[1]);

                return $len;
            });

            $response = Request::curl_exec_follow($curl);
            $info     = \curl_getinfo($curl);
            $status   = (int)$info['http_code'];

            \curl_close($curl);
        } else {
            throw new ServiceProviderException(__('Can not call to remote payment provider'));
        }

        if ($status >= 200 && $status < 300) {
            $decodedResponse = \json_decode($response, false);
            if ($decodedResponse->result === 'ok') {
                $response = $decodedResponse->response;

                if (isset($headers['set-cookie'])) {
                    Frontend::set(self::REQUESTCOOKIE, $headers['set-cookie']);
                }
            } else {
                throw new ServiceProviderException(
                    \sprintf(__('Call to remote payment provider failed with code')
                        . ' (' . $decodedResponse->message->text . ')', $decodedResponse->message->code)
                );
            }
        } else {
            throw new ServiceProviderException(
                \sprintf(__('Call to remote payment provider failed with code'), $status)
            );
        }

        return $response;
    }

    /**
     * @param string $customer
     * @param string $secret
     * @return string
     * @throws ServiceProviderException
     */
    public function checkCustomer(string $customer, string $secret): string
    {
        return $this->httpRequest('checkCustomer', [
            'customer' => $customer,
            'secret'   => $secret,
        ]);
    }

    /**
     * @param string $customerHash
     * @param float  $credit
     * @return bool
     * @throws ServiceProviderException
     */
    public function checkCustomerCredit(string $customerHash, float $credit): bool
    {
        return $this->httpRequest('checkCustomerCredit', [
            'customerHash' => $customerHash,
            'credit'       => $credit,
        ]);
    }

    /**
     * @param string $transactionName
     * @param string $merchantID
     * @param string $merchantSecret
     * @param string $customerHash
     * @param float  $value
     * @return string
     */
    public function getTransaktionToken(
        string $transactionName,
        string $merchantID,
        string $merchantSecret,
        string $customerHash,
        float $value
    ): string {
        return sha1(serialize([$transactionName, $merchantID, $merchantSecret, $customerHash, $value]));
    }

    /**
     * @param string $transactionName
     * @param string $merchantID
     * @param string $merchantSecret
     * @param string $customerHash
     * @param string $notification
     * @param float  $value
     * @param bool   $autoAccept
     * @return string
     * @throws ServiceProviderException
     */
    public function payTransaction(
        string $transactionName,
        string $merchantID,
        string $merchantSecret,
        string $customerHash,
        string $notification,
        float $value,
        bool $autoAccept = false
    ): string {
        $token = $this->getTransaktionToken($transactionName, $merchantID, $merchantSecret, $customerHash, $value);

        return $this->httpRequest('payTransaction', [
            'transactionName' => $transactionName,
            'merchantID'      => $merchantID,
            'customerHash'    => $customerHash,
            'notification'    => $notification,
            'value'           => $value,
            'token'           => $token,
            'autoAccept'      => $autoAccept,
        ]);
    }

    /**
     * @param string $transactionName
     * @param string $merchantID
     * @param string $merchantSecret
     * @param string $customerHash
     * @param string $notification
     * @param float  $value
     * @param bool   $force
     * @return object
     * @throws ServiceProviderException
     */
    public function payPrepaidTransaction(
        string $transactionName,
        string $merchantID,
        string $merchantSecret,
        string $customerHash,
        string $notification,
        float $value,
        bool $force = false
    ): object {
        $token = $this->getTransaktionToken($transactionName, $merchantID, $merchantSecret, $customerHash, $value);

        return $this->httpRequest('payPrepaidTransaction', [
            'transactionName' => $transactionName,
            'merchantID'      => $merchantID,
            'customerHash'    => $customerHash,
            'notification'    => $notification,
            'value'           => $value,
            'token'           => $token,
            'force'           => $force,
        ]);
    }
}
