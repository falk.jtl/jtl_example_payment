<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment;

use Exception;
use JTL\Alert\Alert;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use JTL\Plugin\Data\PaymentMethod as DataPaymentMethod;
use JTL\Plugin\Payment\Method;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_example_payment\frontend\FrontendHandler;
use Plugin\jtl_example_payment\paymentmethod\NotificationInterface;

/**
 * Class Bootstrap
 * @package Plugin\jtl_example_payment
 * @uses \Plugin\jtl_example_payment\paymentmethod\SimplePayment
 * @uses \Plugin\jtl_example_payment\paymentmethod\PrepaidCard
 * @uses \Plugin\jtl_example_payment\paymentmethod\PostpaidCard
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $pluginPath = $this->getPlugin()->getPaths()->getAdminPath();

        try {
            switch ($tabName) {
                case 'Payment method: Simple payment':
                    return $smarty->fetch($pluginPath . 'template/simple_payment.tpl');
                case 'Payment method: Prepaid card':
                    return $smarty->fetch($pluginPath . 'template/prepaid_card.tpl');
                case 'Payment method: Postpaid card':
                    return $smarty->fetch($pluginPath . 'template/postpaid_card.tpl');
            }
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'smartyError'
            );
        }

        return parent::renderAdminMenuTab($tabName, $menuID, $smarty);
    }

    /**
     * @inheritDoc
     * @uses hookCheckoutStepConfirmation
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);

        if (Shop::isFrontend()) {
            $dispatcher->listen(
                'shop.hook.' . \HOOK_BESTELLVORGANG_PAGE_STEPBESTAETIGUNG,
                [$this, 'hookCheckoutStepConfirmation']
            );
        } else {
            $dispatcher->listen('backend.notification', [$this, 'checkPayments']);
        }
    }

    /**
     * Hier werden alle Zahlungsmethoden des Plugin instanziiert, um jedem Modul die Möglichkeit zu geben
     * einen Selbsttest durchzuführen.
     * Diese Methode wird nur im Backend aufgerufen.
     *
     * @return void
     */
    public function checkPayments(): void
    {
        /** @var DataPaymentMethod $paymentMethod */
        foreach ($this->getPlugin()->getPaymentMethods()->getMethods() as $paymentMethod) {
            $method = Method::create($paymentMethod->getModuleID());
            if ($method instanceof NotificationInterface) {
                $method->initBackendNotification();
            }
        }
    }

    /**
     * Handler für den Hook HOOK_BESTELLVORGANG_PAGE_STEPBESTAETIGUNG. Für die Zahlungsmethode "Prepaid Karte"
     * wird ein Guthabentest beim Service-Provider durchgeführt und ggfs. eine Meldung im Frontend angezeigt.
     *
     * @return void
     */
    public function hookCheckoutStepConfirmation(): void
    {
        $payment = Frontend::get('Zahlungsart');
        if (\is_object($payment) && !empty($payment->cModulId)) {
            FrontendHandler::getInstance(
                $this->getPlugin(),
                Shop::Container()->getLinkService(),
                Shop::Container()->getAlertService()
            )->handleCheckoutConfirmation(Method::create($payment->cModulId));
        }
    }
}
