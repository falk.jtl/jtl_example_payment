# JTL Beispiel Zahlungs-Plugin

Achtung! - Dieses Plugin dient nur als Beispiel für Entwickler! Dies ist kein echtes Zahlungs-Plugin!
In diesem Plugin werden verschiedene Zahlungsarten implementiert, die Entwicklern von Zahlungs-Plugins
als Vorlage und Beispiel für eine Implementierung dienen sollen.
Alle Zahlungsarten sind - im Zusammenspiel mit dem Plugin ["JTL Beispiel Zahlungs-Plugin (Server)"](https://gitlab.com/falk.jtl/jtl_example_payment_srv)
funktionsfähig und können in Testsystemen eingesetzt werden.

## Voraussetzungen

* JTL-Shop5 ab 5.0.0-rc.2

## Einfache Zahlung

Die Zahlungsmethode *"Einfache Zahlung"* demonstriert eine einfache Zahlung durch den Kunden, wie z.B. eine
Banküberweisung.
Bei dieser Zahlungsmethode ist keine Interaktion mit dem Kunden erforderlich. Am Ende des Bestellvorganges wird
lediglich eine E-Mail mit Informationen zur Überweisung verschickt.

The payment method *"Simple payment"* demonstrates a simple payment process like a bank transfer by customer
No customer interaction is needed for this payment. The payment process send an e-mail with instructions for the bank
transfer to the customer.

## Prepaid-Karte

Die Zahlungsmethode *"Prepaid-Karte"* demonstriert eine Prepaid-Zahlung durch den Kunden, wie z.B. bei einer
Prepaid-Karte.
Bei dieser Zahlungsmethode muss sich der Kunde während des Bestellvorganges authentifizieren. Beim Bestellabschluss
wird der Betrag vom Kundenkonto abgebucht.
Für den Test dieser Zahlungsmethode muss das Plugin ["JTL Beispiel Zahlungs-Plugin (Server)"](https://gitlab.com/falk.jtl/jtl_example_payment_srv) installiert und dort
ein Kunde mit Guthaben eingerichtet sein.
Alle Angaben zur Zahlung werden vom Plugin direkt vom Kunden abgefragt und per Server-To-Server-Kommunikation zum
Zahlungsanbieter gesendet. Alle Ergebnisse vom Zahlungs-Anbieter stehen sofort zur Verfügung.

The payment method *"Prepaid card"* demonstrates a prepaid payment by customer, e.g a prepaid card.
The customer must authenticate during checkout. The amount will be debited on finalizing the order.
To test this payment method the plugin "JTL Example Payment (Server)" must be installed and a customer with credit
must be set up.

## Postpaid-Karte

Die Zahlungsmethode *"Postpaid-Karte"* demonstriert eine Zahlung mit externem Login beim Zahlungsanbieter und einer
zeitversetzten Zahlungsbestätigung.
Bei dieser Zahlungsmethode muss sich der Kunde während des Bestellvorganges auf der Seite des Zahlungsanbieters
authentifizieren. Beim Bestellabschluss wird ein Zahlvorgang beim Anbieter gestartet.
Nach Bestätigun beim Zahlungsanbieter wird der Zahlungsstatus zurück an den Shop übertragen.
Für den Test dieser Zahlungsmethode muss das Plugin ["JTL Beispiel Zahlungs-Plugin (Server)"](https://gitlab.com/falk.jtl/jtl_example_payment_srv) installiert und dort ein
Kunde eingerichtet sein.
Die für die Zahlung notwendigen Kundenaktionen werden direkt beim Zahlungsanbieter ausgeführt. Das Plugin führt dazu
http-Weiterleitungen zum Zahlungsanbieter aus und übermittelt neben den Transaktionsinformationen immer auch eine
Callback-URL für den Rücksprung zum Shop.
Die Ergebnisse werden vom Zahlungsanbieter per Callback-URL (Webhook) an den Shop übermittelt. Diese Aufrufe
können (z.B. bei Bestätigung der Zahlung) beliebig zeitversetzt erfolgen.

The payment method *"Postpaid card"* demonstrates a paymenth with external login at the payment provider and a delayed
confirmation.
The customer must authenticate during checkout at the payment provider page. During checkout a payment transaction is
created at payment provider.
After confirmation by payment provider the state will be send back to the shop.
To test this payment method the plugin "JTL Example Payment (Server)" must be installed and a customer must be set up.

## Related Links

* [JTL Beispiel Zahlungs-Plugin](https://gitlab.com/falk.jtl/jtl_example_payment)
* [JTL Beispiel Zahlungs-Plugin (Server)](https://gitlab.com/falk.jtl/jtl_example_payment_srv)