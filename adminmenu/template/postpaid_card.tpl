<div class="description mb-4">
    <p><strong>{__('JTL Example Payment')}</strong></p>
    <p class="alert alert-warning">{__('JTL Example Payment - This is for developers only! This is not a real payment plugin!')}</p>
    <p>{__('The payment method Postpaid card demonstrate a postpaid payment process.')}</p>
</div>