<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\frontend;

use JTL\Alert\Alert;
use JTL\Plugin\Payment\MethodInterface;
use JTL\Plugin\PluginInterface;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Services\JTL\LinkServiceInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_example_payment\paymentmethod\PrepaidCard;

/**
 * Class FrontendHandler
 * @package Plugin\jtl_example_payment\frontend
 */
class FrontendHandler
{
    /** @var PluginInterface */
    protected $plugin;

    /** @var LinkServiceInterface */
    protected $linkService;

    /** @var AlertServiceInterface */
    protected $alertService;

    /**
     * FrontendHandler constructor.
     * @param PluginInterface       $plugin
     * @param LinkServiceInterface  $linkService
     * @param AlertServiceInterface $alertService
     */
    public function __construct(
        PluginInterface $plugin,
        LinkServiceInterface $linkService,
        AlertServiceInterface $alertService
    ) {
        $this->plugin       = $plugin;
        $this->linkService  = $linkService;
        $this->alertService = $alertService;
    }

    /**
     * @param PluginInterface       $plugin
     * @param LinkServiceInterface  $linkService
     * @param AlertServiceInterface $alertService
     * @return static
     */
    public static function getInstance(
        PluginInterface $plugin,
        LinkServiceInterface $linkService,
        AlertServiceInterface $alertService
    ): self {
        return new static($plugin, $linkService, $alertService);
    }

    /**
     * @param MethodInterface $method
     * @return void
     */
    public function handleCheckoutConfirmation(?MethodInterface $method): void
    {
        if (!($method instanceof PrepaidCard)
            || $method->validateCredit(Frontend::getCart()->gibGesamtsummeWaren())
        ) {
            return;
        }

        $this->alertService->addAlert(
            Alert::TYPE_INFO,
            $this->plugin->getLocalization()->getTranslation('prepayed_payment_not_enough_credit')
            ?? __('Not enough credit on the prepaid card'),
            'notEnoughCredit',
            [
                'LinkText'      => Shop::Lang()->get('modifyPaymentOption', 'checkout'),
                'LinkHref'      => $this->linkService->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1',
                'SaveInSession' => true,
            ]
        );
    }
}
