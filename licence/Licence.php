<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 * @package       jtl-shop
 */

namespace Plugin\jtl_example_payment\licence;

use JTL\Plugin\LicenseInterface;

/**
 * Class ExamplePaymentLicence
 * @package Plugin\jtl_example_payment\licence
 */
class Licence implements LicenseInterface
{
    /**
     * @inheritDoc
     */
    public function checkLicence($cLicence)
    {
        return $cLicence === 'test';
    }
}
